import datetime
import math
import random
from io import BytesIO
from typing import Optional

import discord
import tabulate
from redbot.core import Config
from redbot.core import commands, checks
from redbot.core.commands import Context

from .compliments import COMPLIMENTS
from .drawprofile import draw_profile


class Forum(commands.Cog):
    """ Forum levelling cog"""

    def __init__(self, bot):
        super(Forum, self).__init__()

        self.config = Config.get_conf(self, identifier=112263)

        self.bot = bot

        default_guild = {
            "xps": {
                "short": 5,
                "medium": 10,
                "long": 15,
                "extreme": 0,
                "image": 10,
            },
            "lengths": {
                "short": 50,
                "medium": 140,
                "long": 600,
            },
            "levels": {
                "base_xp": 100,
                "xp_increase": 10,
                "max_xp": 500,
            },
            "xp_randomness_percents": 20,
            "giveaway": {
                "min": 10,
                "max": 50,
                "channel": None,
            },
            "colour": "#03a1fc",
            "slowdown": 30,
        }
        self.valid_names = set()
        for k, v in default_guild.items():
            if not isinstance(v, dict):
                self.valid_names.add(k)
            else:
                self.valid_names.update([f"{k}.{w}" for w in v.keys()])

        self.config.register_guild(**default_guild)

        self.default_member = {
            "xps": 0,
            "total_xps": 0,
            "level": 0,
            "messages": 0,
            "counted_messages": 0,
            "attachments": 0,
            "last_counted": None,
        }

        self.config.register_member(**self.default_member)

        default_channel = {
            "ignored": False
        }

        self.config.register_channel(**default_channel)

    @commands.command()
    @checks.admin_or_permissions(manage_guild=True)
    async def forum_config(self, ctx):
        """Lists current Forum configuration"""
        values = await self.config.guild(ctx.guild).all()

        message = f"""
        The leveler is currently configured in the following way:
            - `xps.short`: {values["xps"]["short"]} 
            - `xps.medium`: {values["xps"]["medium"]} 
            - `xps.long`: {values["xps"]["long"]} 
            - `xps.extreme`: {values["xps"]["extreme"]} 
            - `xps.image`: {values["xps"]["image"]} 
            - `lengths.short`: {values["lengths"]["short"]} 
            - `lengths.medium`: {values["lengths"]["medium"]} 
            - `lengths.long`: {values["lengths"]["long"]} 
            - `levels.base_xp`: {values["levels"]["base_xp"]} 
            - `levels.xp_increase`: {values["levels"]["xp_increase"]} 
            - `levels.max_xp`: {values["levels"]["max_xp"]} 
            - `giveaway.min`: {values["giveaway"]["min"]} 
            - `giveaway.max`: {values["giveaway"]["max"]} 
            - `giveaway.channel`: {values["giveaway"]["channel"]} 
            - `xp_randomness_percents`: {values["xp_randomness_percents"]}
            - `colour`: {values['colour']}
            - `slowdown`: {values['slowdown']}
        """
        await ctx.send(message)

    @commands.command()
    @checks.admin_or_permissions(manage_guild=True)
    async def forum_config_set(self, ctx, name, value):
        """Updates forum's configuration"""

        try:
            if name == "colour":
                self.parse_colour(value)
            elif name == "giveaway.channel":
                value = value.replace("#", "").replace("<", "").replace(">", "")

                if discord.utils.get(ctx.guild.channels, id=int(value)) is None:
                    raise ValueError
            else:
                value = int(value)

        except (ValueError, IndexError):
            if name == "colour":
                await ctx.send("Please provide a valid HEX color.")
            elif name == "giveaway.channel":
                await ctx.send("Please provide a valid channel")
            else:
                await ctx.send("Please provide a integer.")

            return

        attr = self.config.guild(ctx.guild)

        try:
            if name not in self.valid_names:
                raise Exception
            if "." in name:
                first, second = name.split(".")
                attr = getattr(getattr(attr, first), second)
            else:
                attr = getattr(attr, name)
        except Exception:
            await ctx.send("Invalid config name.")
            return

        await attr.set(value)

        await ctx.send(f"Value of {name} was set to {value}")

    async def xps_for_level(self, guild, level) -> int:
        guild_config = await self.config.guild(guild).all()

        xps = guild_config["levels"]["base_xp"] + (level - 1) * guild_config["levels"]["xp_increase"]

        return min(xps, guild_config["levels"]["max_xp"])

    def xps_for_level_from_config(self, guild_config, level) -> int:
        xps = guild_config["levels"]["base_xp"] + (level - 1) * guild_config["levels"]["xp_increase"]

        return min(xps, guild_config["levels"]["max_xp"])

    def parse_colour(self, c):
        c = c.lstrip("#")

        return tuple(int(c[i:i + 2], 16) for i in (0, 2, 4))

    async def get_colour(self, ctx):
        colour = await self.config.guild(ctx.guild).colour()

        colour = self.parse_colour(colour)

        return discord.Colour.from_rgb(*colour)

    @commands.command()
    async def level(self, ctx: Context, user: discord.Member = None):
        """ Prints the level of the user
        """
        if user:
            member = user
        else:
            member = ctx.author
        guild = ctx.guild

        member_config = await self.config.member(member).all()
        level = member_config["level"]
        xps = member_config["xps"]

        next_xps = await self.xps_for_level(guild, level + 1)
        percentage = xps / next_xps

        rank = await self.get_rank(ctx, member)

        img = await draw_profile(member.avatar_url_as(format="png", size=256),
                                 member.display_name,
                                 member.name if member.nick != "" else "",
                                 rank,
                                 level,
                                 percentage,
                                 str(xps),
                                 str(next_xps),
                                 await self.get_colour(ctx))

        image = BytesIO()
        img.save(image, "PNG")
        image.seek(0)

        await ctx.message.delete()
        await ctx.send(file=discord.File(image, filename="level.png"))

    async def get_leaderboard(self, ctx):
        members = ctx.guild.members

        list_of_members = []

        for member in members:
            if member.bot:
                continue
            member_config = await self.config.member(member).all()

            if not member_config["total_xps"]:
                continue

            list_of_members.append((
                member, member_config
            ))

        list_of_members.sort(key=lambda x: (x[1]["level"], x[1]["total_xps"]), reverse=True)

        list_of_members = [[i + 1] + list(x) for i, x in enumerate(list_of_members)]

        return list_of_members

    async def get_rank(self, ctx, member):
        leaderboard = await self.get_leaderboard(ctx)

        rank = 0
        for rank, m, *_ in leaderboard:
            if member == m:
                break

        return rank

    def format_total_xps(self, xps):
        if xps < 1000:
            return str(xps)
        for unit in ['', 'k', 'M', 'G']:
            if abs(xps) < 1000:
                return "%.1f%s" % (xps, unit)
            xps /= 1000
        return "%.1f%s" % (xps, 'Gi')

    @commands.command()
    async def leaderboard(self, ctx: Context, show_messages: bool = False):
        """ Prints the Forum leaderboard"""
        guild_config = await self.config.guild(ctx.guild).all()

        list_of_members = await self.get_leaderboard(ctx)

        table = []

        for rank, member, member_config in list_of_members:
            level = member_config["level"]
            xps = member_config["xps"]
            total_xps = member_config["total_xps"]
            messages = member_config["messages"]

            next_xps = self.xps_for_level_from_config(guild_config, level + 1)

            place = str(rank)

            progress = str(int(xps / next_xps * 100)) + "%"

            table.append(
                [place, member.display_name, level, self.format_total_xps(total_xps),
                 messages if show_messages else progress]
            )

        tabulate.MIN_PADDING = 0
        message = tabulate.tabulate(table,
                                    ["", "User", "Lvl", "XP", "Msgs" if show_messages else "Prgrs"],
                                    tablefmt="fancy_grid",
                                    numalign="right",
                                    stralign="right",)

        await ctx.message.delete()
        await ctx.send(f"**Forum Leaderboard**\n```{message}```")

    @commands.command()
    @checks.admin_or_permissions(manage_guild=True)
    async def forum_ignore(self, ctx, channel: discord.TextChannel = None):
        """Toggles ignore of the channel by the leveler"""
        if channel is None:
            channel = ctx.channel

        ignored = await self.config.channel(channel).ignored()

        new_ignored = not ignored

        await self.config.channel(channel).ignored.set(new_ignored)

        if ignored:
            await ctx.send("The channel will now be included in levelling.")
        else:
            await ctx.send("The channel will now be ignored from levelling.")

    @commands.command()
    @checks.admin_or_permissions(manage_guild=True)
    async def forum_reset(self, ctx, user: discord.Member = None):
        """ Resets level and XP of all users (or a specific user)
        """
        if user is None:
            users = ctx.guild.members
        else:
            users = [user]

        for u in users:
            for k, v in self.default_member.items():
                await getattr(self.config.member(u), k).set(v)

        if user:
            await ctx.send(f"The progress of {user.display_name} has been reset.")
        else:
            await ctx.send("The progress of all users has been reset.")

    @commands.command()
    @checks.admin_or_permissions(manage_guild=True)
    async def forum_recalc_levels(self, ctx):
        """ Recalculates the levels based on total XP
        """
        guild_config = await self.config.guild(ctx.guild).all()

        for user in ctx.guild.members:
            total_xps = await self.config.member(user).total_xps()
            level = 0

            while True:
                next_xps = self.xps_for_level_from_config(guild_config, level + 1)

                if total_xps > next_xps:
                    level += 1
                    total_xps -= next_xps
                    if total_xps < 0:
                        break
                else:
                    break

            await self.config.member(user).xps.set(total_xps)
            await self.config.member(user).level.set(level)

        await ctx.send("The levels were recalculated.")

    def ordinal(self, n):
        return "%d%s" % (n, "tsnrhtdd"[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4])

    async def listener(self, message: discord.Message):
        if type(message.author) != discord.Member:
            # throws an error when webhooks talk, this fixes it
            return
        if type(message.channel) != discord.channel.TextChannel:
            return
        if message.author.bot:
            return

        if message.content and message.content[0] in await self.bot.get_prefix(message):
            return

        if await self.config.channel(message.channel).ignored():
            return

        member = message.author
        member_config = await self.config.member(member).all()

        guild = message.guild
        config = await self.config.guild(guild).all()
        slowdown_seconds = config["slowdown"]

        created_at: datetime.datetime = message.created_at
        last_counted: Optional[str] = member_config["last_counted"]

        if last_counted is not None:
            last_counted_datetime = datetime.datetime.fromisoformat(last_counted)
            if (created_at - last_counted_datetime).total_seconds() < slowdown_seconds:
                await self.config.member(member).messages.set(member_config["messages"] + 1)
                return

        content = message.content
        content_length = len(content)

        messages = member_config["messages"]
        counted_messages = member_config["counted_messages"]
        attachments = member_config["attachments"]

        if content_length == 0:
            base_xps = 0
        else:
            for category in ["short", "medium", "long"]:
                if content_length <= config["lengths"][category]:
                    message_category = category
                    break
            else:
                message_category = "extreme"

            base_xps = config["xps"][message_category]

            messages += 1
            counted_messages += 1

        # image adds more XP
        if len(message.attachments) or len(message.embeds):
            base_xps += config["xps"]["image"]
            attachments += 1

        percents = config["xp_randomness_percents"]

        xps_added = math.ceil(base_xps * random.uniform(1 - percents / 100, 1 + percents / 100))

        await self.config.member(member).last_counted.set(created_at.isoformat())
        await self.config.member(member).messages.set(messages)
        await self.config.member(member).counted_messages.set(counted_messages)
        await self.config.member(member).attachments.set(attachments)

        await self.add_xps(message, member, xps_added)

    async def add_xps(self, ctx, member, xps_to_add, channel=None):
        if channel is None:
            channel = ctx.channel

        member_config = await self.config.member(member).all()

        new_level = level = member_config["level"]
        new_xps = member_config["xps"] + xps_to_add
        new_total_xps = member_config["total_xps"] + xps_to_add

        next_xps = await self.xps_for_level(ctx.guild, level + 1)

        if new_xps > next_xps:
            new_level += 1
            new_xps -= new_xps
            new_xps = int(new_xps)

        await self.config.member(member).xps.set(new_xps)
        await self.config.member(member).level.set(new_level)
        await self.config.member(member).total_xps.set(new_total_xps)

        if level != new_level:
            rank = await self.get_rank(ctx, member)
            position = self.ordinal(rank)
            medal = {1: ":first_place:", 2: ":second_place:", 3: ":third_place:"}.get(rank, "")

            text = (f"**Forum Level Advancement!**\n"
                    f":tada: Congrats! {member.mention} just advanced to **level {new_level}**. \n"
                    f"They are in **{position} position** in total ranking{medal}. :tada:")

            await channel.send(text)

    def lower_first(self, s):
        return s[:1].lower() + s[1:] if s else ''

    @commands.command()
    async def forum_giveaway(self, ctx: Context):
        """ Resets level and XP of all users (or a specific user)
        """
        if not (ctx.message.author.bot or await ctx.bot.is_owner(ctx.message.author)):
            print("eh?")
            return

        user = random.choice([x for x in ctx.guild.members if not x.bot])

        g_min = await self.config.guild(ctx.guild).giveaway.min()
        g_max = await self.config.guild(ctx.guild).giveaway.max()
        g_channel = await self.config.guild(ctx.guild).giveaway.channel()

        if g_channel is None:
            return

        xps_to_add = random.randint(g_min, g_max)

        compliment = self.lower_first(random.choice(COMPLIMENTS))

        text = (f"**Forum Giveaway!**\n"
                f"Hey {user.mention}, {compliment} Here, have {xps_to_add} XP!")

        channel = discord.utils.get(ctx.guild.channels, id=int(g_channel))

        await channel.send(text)
        await self.add_xps(ctx, user, xps_to_add, channel=channel)
